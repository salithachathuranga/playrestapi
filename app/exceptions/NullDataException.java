package exceptions;

public class NullDataException extends RuntimeException {

    public NullDataException(String msg) {
        super(msg);
    }
}
