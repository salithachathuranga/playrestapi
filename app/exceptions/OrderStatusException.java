package exceptions;

public class OrderStatusException extends RuntimeException {

    public OrderStatusException(String msg) {
        super(msg);
    }
}
